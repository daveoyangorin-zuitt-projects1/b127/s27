let http = require('http');

let user = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
		},
		{
		"firstName": "John",
		"lastName":"Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
		}
	]

http.createServer(function(req , res){
	if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200,{'Content-Type': 'application/json'});
		res.write(JSON.stringify(user));
		res.end()
	}

	if(req.url == "/register" && req.method == "POST"){
		let requestBody = '';
		req.on('data', function(data){
			requestBody += data
		})

		req.on('end', function(){
		console.log(typeof requestBody);

				requestBody = JSON.parse(requestBody);

				let newUser = {
					"firstName": requestBody.firstName,
					"lastName": requestBody.lastName,
					"mobileNo": requestBody.mobileNo,
					"email": requestBody.email,
					"password": requestBody.password
				}

				user.push(newUser)
				console.log(user)

				res.writeHead(200,{'Content-Type': 'application/json'});
				res.write(JSON.stringify(newUser))
				res.end()
			})
		}

}).listen(5000);

console.log('CRUD server running at localhost:5000')